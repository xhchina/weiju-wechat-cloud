export const noLoadingRequest = async function(header,params){
  const {name,api,isUser} = header
  const userInfo = wx.getStorageSync('wjUser')
  let paramsData = params
  if(isUser && !userInfo._openid){
    return wx.navigateTo({
      url: '/pages/welcome/welcome'
    })
  }else{
    paramsData = Object.assign(params,{_openid:userInfo._openid})
  }
  const {result} = await wx.cloud.callFunction({
    name:name,
    data:{
      api:api,
      data:paramsData
    }
  })
  return result
}

export const loadingRequest = async function(header,params){
  wx.showLoading({
    title: '正在加载...'
  })
  const {name,api,isUser} = header
  const userInfo = wx.getStorageSync('wjUser')
  let paramsData = params
  if(isUser && !userInfo._openid){
    return wx.navigateTo({
      url: '/pages/welcome/welcome'
    })
  }else{
    paramsData = Object.assign(params,{_openid:userInfo._openid})
  }
  const {result} = await wx.cloud.callFunction({
    name:name,
    data:{
      api:api,
      data:paramsData
    }
  })
  wx.hideLoading()
  return result
}