// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const _ = db.command

// 分页查询聊天记录
const queryPageWjChatRecord = async function (params) {
  const wxContext = cloud.getWXContext()

  let chatId = params.chatId
  let pageNO = params.pageNO || 1
  let pageSize = params.pageSize || 10


  try {

    let records = await db.collection('chatRecord').orderBy('createTime', 'asc').where({
      chatId: chatId,
    }).skip((pageNO - 1) * pageSize).limit(pageSize).get();
    records = records.data

    return records

  } catch (error) {

    console.log('queryPageWjChatRecord error' + error)

    return []
  }

}

module.exports = queryPageWjChatRecord