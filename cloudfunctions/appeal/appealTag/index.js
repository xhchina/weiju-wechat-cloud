// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()

// 诉求 保存
 const appealTag = async function (params) {
  const wxContext = cloud.getWXContext()

  let result = await db.collection('appealTag').get()

  return result.data

}

module.exports = appealTag